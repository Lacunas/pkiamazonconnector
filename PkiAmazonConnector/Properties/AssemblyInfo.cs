﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Lacuna PKI Amazon Connector")]
[assembly: AssemblyDescription("Enables integration between the Lacuna PKI SDK and Amazon Web Services.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Lacuna Software")]
[assembly: AssemblyProduct("Lacuna PKI SDK")]
[assembly: AssemblyCopyright("Copyright © 2015 Lacuna Software")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6e3f26a0-adfa-4709-8b0d-f979eb4a3265")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.0")]
[assembly: AssemblyFileVersion("1.2.0")]
[assembly: AssemblyInformationalVersion("1.2.0")] // used by nuget to determine the package version
