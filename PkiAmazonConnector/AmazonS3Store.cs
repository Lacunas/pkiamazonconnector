﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Lacuna.Pki.Stores;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.AmazonConnector {

	public class AmazonS3Store : ISimpleStore {

		public static AmazonS3Store CreateFromAccessCredentials(string awsAccessKey, string awsSecretAccessKey, RegionEndpoint regionEndpoint, string bucketName, string basePath = null) {
			var client = AWSClientFactory.CreateAmazonS3Client(awsAccessKey, awsSecretAccessKey, new AmazonS3Config() {
				RegionEndpoint = regionEndpoint
			});
			return CreateFromS3Client(client, bucketName, basePath);
		}

		public static AmazonS3Store CreateFromS3Client(IAmazonS3 s3Client, string bucketName, string basePath = null) {
			return new AmazonS3Store(s3Client, bucketName, basePath);
		}

		private IAmazonS3 s3Client;
		private string bucketName;
		private string basePath;

		private AmazonS3Store(IAmazonS3 s3Client, string bucketName, string basePath) {
			this.s3Client = s3Client;
			this.bucketName = bucketName;
			this.basePath = basePath;
		}

		public byte[] Get(byte[] index) {
			var request = new GetObjectRequest() {
				BucketName = bucketName,
				Key = getObjectKey(index)
			};
			GetObjectResponse response;
			try {
				response = s3Client.GetObject(request);
			} catch (AmazonS3Exception ex) {
				if (ex.ErrorCode == "NoSuchKey") {
					return null;
				} else {
					throw;
				}
			}
			byte[] content;
			using (var buffer = new MemoryStream()) {
				response.ResponseStream.CopyTo(buffer);
				content = buffer.ToArray();
			}
			return content;
		}

		public void Put(byte[] index, byte[] content) {
			using (var buffer = new MemoryStream(content)) {
				var request = new PutObjectRequest() {
					BucketName = bucketName,
					Key = getObjectKey(index),
					InputStream = buffer
				};
				s3Client.PutObject(request);
			}
		}

		private string getObjectKey(byte[] index) {
			//return Convert.ToBase64String(index).Replace("+", "%2B").Replace("=", "%3D");
			var key = String.Join("", index.Select(b => b.ToString("X2")));
			if (String.IsNullOrEmpty(basePath)) {
				return key;
			} else {
				return String.Format("{0}/{1}", basePath, key);
			}
		}
	}
}
