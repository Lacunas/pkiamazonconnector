﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Lacuna.Pki.Cades;
using Amazon;
using Lacuna.Pki.Stores;
using System.Text;
using System.Configuration;
using Amazon.S3;
using Amazon.S3.Model;

namespace Lacuna.Pki.AmazonConnector.Tests {

	[TestClass]
	public class AmazonS3StoreTests {

		// Change this if your S3 bucket is in another region
		private static readonly RegionEndpoint region = RegionEndpoint.SAEast1;

		private IAmazonS3 s3Client;
		private string bucketName;
		private string basePath;

		[TestInitialize]
		public void TestInitialize() {

			var awsAccessKey = ConfigurationManager.AppSettings["AccessKeyId"];
			var awsSecretAccessKey = ConfigurationManager.AppSettings["AccessKeySecret"];
			this.s3Client = AWSClientFactory.CreateAmazonS3Client(awsAccessKey, awsSecretAccessKey, new AmazonS3Config() {
				RegionEndpoint = region
			});

			this.bucketName = ConfigurationManager.AppSettings["BucketName"];
			this.basePath = String.Format("pki-amazon-connector-test/{0}", DateTime.Now.Ticks);
		}

		[TestCleanup]
		public void TestCleanup() {
			var listObjectsRequest = new ListObjectsRequest() {
				BucketName = bucketName,
				Prefix = basePath + "/"
			};
			var listObjectsResponse = s3Client.ListObjects(listObjectsRequest);
			var deleteObjectsRequest = new DeleteObjectsRequest() {
				BucketName = bucketName,
				Objects = listObjectsResponse.S3Objects.ConvertAll(o => new KeyVersion() { Key = o.Key })
			};
			s3Client.DeleteObjects(deleteObjectsRequest);
		}

		[TestMethod]
		public void CompressAndDecompress() {

			for (int i = 1; i <= 2; i++) {
				var store = AmazonS3Store.CreateFromS3Client(s3Client, bucketName, basePath);
				byte[] precomputedSignature = File.ReadAllBytes(String.Format("Signature{0}.p7s", i));
				var compressedSignature = CadesSignatureCompression.Compress(precomputedSignature, store);
				var decompressedSignature = CadesSignatureCompression.Decompress(compressedSignature, store);
				CollectionAssert.AreEqual(precomputedSignature, decompressedSignature);
			}

			// Both signatures contain the same 3 certificates and 2 CRLs, so the bucket folder should
			// have only 5 objects, any more than that means unecessary redundancy

			var listObjectsRequest = new ListObjectsRequest() {
				BucketName = bucketName,
				Prefix = basePath + "/"
			};
			var response = s3Client.ListObjects(listObjectsRequest);
			Assert.AreEqual(5, response.S3Objects.Count);
		}

	}
}
